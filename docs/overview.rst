
mosaik-heatpump
===============

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:

   models/index
   example_scenarios
   advanced/index

mosaik-heatpump (|mosaik_hp_version|) provides models for the simulation of heating systems- consisting of heat pumps, hot water tanks,
and controllers - and adapters for the co-simulation of these models using *mosaik*.

.. rubric:: Installation & Tests

You can install mosaik-heatpump with pip:

.. code-block:: bash

      pip install mosaik-heatpump


You can run the tests with:

.. code-block:: bash

      pytest

.. rubric:: Getting started


A description of the different models available in the package and examples to use individual models can
be found :doc:`here <./models/index>`.

Example scenarios for the co-simulation of all the models can be found :doc:`here <./example_scenarios>`.

.. rubric:: Getting help


Please report bugs and ideas for improvement to our `issue tracker`__.

__ https://gitlab.com/mosaik/components/energy/mosaik-heatpump/-/issues

For questions and general discussion about mosaik-heatpump, you can use
`mosaik's GitHub Discussions <https://github.com/orgs/OFFIS-mosaik/discussions>`_.


.. rubric:: Citation

The model was initially used and presented `here <https://doi.org/10.1186/s42162-021-00180-6>`_ , but it is not
described in detail. We plan to publish a paper presenting the package, and will update this
section after the publication.

.. rubric:: License

The package is completely open source and is covered under the `MIT License. <https://gitlab.com/mosaik/
components/energy/mosaik-heatpump/-/blob/master/LICENSE?ref_type=heads>`_

