from mosaik_components.heatpump.Heat_Pump_mosaik import HeatPumpSimulator
import mosaik
import os
import pytest
import pandas as pd


sim_config = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'CSV_writer': {
        'python': 'mosaik_csv_writer:CSVWriter'
    },
    'HeatPumpSim': {
        'python': 'mosaik_components.heatpump.Heat_Pump_mosaik:HeatPumpSimulator',
    },
    'HotWaterTankSim': {
        'python': 'mosaik_components.heatpump.hotwatertank.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'ControllerSim': {
        'python': 'mosaik_components.heatpump.controller.controller_mosaik:ControllerSimulator',
    },
}

def example_scenario(same_time_loop):
    # The start date, duration, and step size for the simulation
    END = 10 * 60
    START = '01.01.2020 00:00'
    STEP_SIZE = 60 * 1

    # Parameters for mosaik-heatpump
    params_hp = {'hp_model': 'Air_30kW_1stage',
                 'heat_source': 'Air',
                 'cons_T': 35,
                 'Q_Demand': 19780,
                 'cond_in_T': 30,
                 'heat_source_T': 7,
                 }
    # Parameters for hot water tank model
    params_hwt = {
        'height': 3600,
        'volume': 4000,
        'T_env': 20.0,
        'htc_walls': 0.28,
        'htc_layers': 0.897,
        'n_layers': 6,
        'n_sensors': 6,
        'connections': {
            'sh_in': {'pos': 10},
            'sh_out': {'pos': 2150},
            'dhw_in': {'pos': 10},
            'dhw_out': {'pos': 3400},
            'hp_in': {'pos': 10},
            'hp_out': {'pos': 500},
        },
    }
    init_vals_hwt = {
        'layers': {'T': [40.0, 40.0, 40.0, 40.0, 40.0, 40.0]}
    }
    # Parameters for controller model
    params_ctrl = {
        'T_hp_sp_h': 50,
        'T_hp_sp_l': 40,
        'T_hr_sp_dhw': 40,
        'T_hr_sp_sh': 35,
        'dhw_in_T': 10,
        'sh_dT': 7,
        'operation_mode': 'heating',
        'control_strategy': '1'
    }

    # The different types of heat pumps and calculation modes that are simulated
    model_list = ['Air_30kW_1stage', 'Air_30kW_1stage', 'LW 300(L)', None]
    calc_mode_list = ['detailed', 'fast', 'hplib', 'fixed']
    filename_list = ['detailed', 'fast', 'hplib', 'fixed']

    for i in range(len(model_list)):

        # Initialize the world and the simulators.

        world = mosaik.World(sim_config)

        with world.group():
            heatpumpsim = world.start('HeatPumpSim', step_size=STEP_SIZE, same_time_loop=same_time_loop)

            hwtsim = world.start('HotWaterTankSim', step_size=STEP_SIZE, config=params_hwt,
                             same_time_loop=same_time_loop)

            ctrlsim = world.start('ControllerSim', step_size=STEP_SIZE, same_time_loop=same_time_loop)

        heat_load_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'docs', 'code',
                                      'examples', 'data', 'scenario_data.csv')

        heat_load_sim = world.start('CSV', sim_start=START, datafile=heat_load_file)

        # CSV_File = 'Scenario_' + filename_list[i] + '_time_shifted.csv'
        # csv_sim_writer = world.start('CSV_writer', start_date='01.01.2020 00:00', date_format='%d.%m.%Y %H:%M',
        #                              output_file=CSV_File)

        params_hp['calc_mode'] = calc_mode_list[i]
        params_hp['hp_model'] = model_list[i]

        if 'hplib' in params_hp['calc_mode']:
            params_hp['equivalent_hp_model'] = 'Air_30kW_1stage'
        elif 'fixed' in params_hp['calc_mode']:
            params_hp['COP'] = 3.5
            params_hp['heating capacity'] = 15000
            params_hp['cond_m'] = 0.5

        # Instantiate the models
        heatpumps = heatpumpsim.HeatPump.create(1, params=params_hp)

        hwts = hwtsim.HotWaterTank.create(1, params=params_hwt, init_vals=init_vals_hwt)

        ctrls = ctrlsim.Controller.create(1, params=params_ctrl)

        heat_load = heat_load_sim.HEATLOAD.create(1)

        # csv_writer = csv_sim_writer.CSVWriter(buff_size=60 * 60)

        # connections between the different models

        if not same_time_loop:
            world.connect(heat_load[0], ctrls[0], 'T_amb', ('T_amb', 'heat_source_T'), ('SH Demand [kW]', 'sh_demand'),
                          ('DHW Demand [L]', 'dhw_demand'), 'dhw_in_T')

            world.connect(hwts[0], ctrls[0], ('T_mean', 'T_mean_hwt'), ('mass', 'hwt_mass'),
                          ('sensor_00.T', 'bottom_layer_T'), ('sensor_04.T', 'top_layer_T'),
                          ('dhw_out.T', 'dhw_out_T'), ('sh_out.T', 'sh_out_T'), ('hp_out.T', 'hp_out_T'))

            world.connect(ctrls[0], hwts[0], ('sh_in_F', 'sh_in.F'), ('sh_in_T', 'sh_in.T'), ('sh_out_F', 'sh_out.F'),
                          ('dhw_in_F', 'dhw_in.F'), ('dhw_in_T', 'dhw_in.T'), ('dhw_out_F', 'dhw_out.F'),
                          ('T_amb', 'T_env'),
                          time_shifted=True,
                          initial_data={'sh_in_F': 0, 'sh_in_T': 0, 'sh_out_F': 0,
                                        'dhw_in_F': 0, 'dhw_in_T': 0, 'dhw_out_F': 0,
                                        'T_amb': 0,
                                        },
                          )

            world.connect(heatpumps[0], ctrls[0], ('Q_Supplied', 'hp_supply'), ('on_fraction', 'hp_on_fraction'),
                          ('cond_m', 'hp_cond_m'))

            world.connect(ctrls[0], heatpumps[0], ('hp_demand', 'Q_Demand'),
                          'T_amb', 'heat_source_T', time_shifted=True,
                          initial_data={'hp_demand': 0, 'T_amb': 5, 'heat_source_T': 5})

            world.connect(hwts[0], heatpumps[0], ('hp_out.T', 'cond_in_T'))

            world.connect(heatpumps[0], hwts[0], ('cons_T', 'hp_in.T'), ('cond_m', 'hp_in.F'), ('cond_m_neg', 'hp_out.F'),
                          time_shifted=True, initial_data={'cons_T': 0, 'cond_m': 0, 'cond_m_neg': 0})

            # world.connect(heat_load[0], csv_writer, 'T_amb', 'SH Demand [kW]', 'DHW Demand [L]')
            # world.connect(heatpumps[0], csv_writer, 'Q_Demand', 'Q_Supplied', 'T_amb', 'heat_source_T', 'cons_T',
            #               'P_Required',
            #               'COP', 'cond_m', 'cond_in_T', 'on_fraction')
            #
            # world.connect(ctrls[0], csv_writer, 'heat_demand', 'heat_supply', 'hp_demand', 'sh_supply', 'sh_demand',
            #               'hp_supply',
            #               'sh_in_F', 'sh_in_T', 'sh_out_F', 'sh_out_T', 'dhw_in_F', 'dhw_in_T', 'dhw_out_F', 'dhw_out_T',
            #               'hp_in_F', 'hp_in_T', 'hp_out_F', 'hp_out_T', 'P_hr_sh', 'P_hr_dhw', 'dhw_demand', 'dhw_supply')
            # world.connect(hwts[0], csv_writer, 'sensor_00.T', 'sensor_01.T', 'sensor_02.T', 'sensor_03.T', 'sensor_04.T',
            #               'sensor_05.T', 'sh_out.T', 'sh_out.F', 'dhw_out.T', 'dhw_out.F', 'hp_in.T', 'hp_in.F', 'hp_out.T',
            #               'hp_out.F',
            #               'T_mean', 'sh_in.T', 'sh_in.F', 'dhw_in.T', 'dhw_in.F')

        else:

            world.connect(heat_load[0], ctrls[0], 'T_amb', ('T_amb', 'heat_source_T'), ('SH Demand [kW]', 'sh_demand'),
                          ('DHW Demand [L]', 'dhw_demand'), 'dhw_in_T')

            world.connect(hwts[0], ctrls[0], ('T_mean', 'T_mean_hwt'), ('mass', 'hwt_mass'),
                          ('sensor_00.T', 'bottom_layer_T'), ('sensor_04.T', 'top_layer_T'),
                          ('dhw_out.T', 'dhw_out_T'), ('sh_out.T', 'sh_out_T'),
                          ('hp_out.T', 'hp_out_T'))

            world.connect(ctrls[0], hwts[0], ('sh_in_F', 'sh_in.F'), ('sh_in_T', 'sh_in.T'), ('sh_out_F', 'sh_out.F'),
                          ('dhw_in_F', 'dhw_in.F'), ('dhw_in_T', 'dhw_in.T'), ('dhw_out_F', 'dhw_out.F'),
                          ('T_amb_hwt', 'T_env'),
                          ('hp_in_T', 'hp_in.T'), ('hp_in_F', 'hp_in.F'), ('hp_out_F', 'hp_out.F'), weak=True)

            world.connect(heatpumps[0], ctrls[0], ('Q_Supplied', 'hp_supply'), ('on_fraction', 'hp_on_fraction'),
                          ('cond_m', 'hp_in_F'), ('cond_m_neg', 'hp_out_F'), ('cons_T', 'hp_in_T'), weak=True)

            world.connect(ctrls[0], heatpumps[0], ('hp_demand', 'Q_Demand'), ('hp_out_T', 'cond_in_T'),
                          'T_amb', 'heat_source_T')

            # world.connect(heat_load[0], csv_writer, 'T_amb', 'SH Demand [kW]', 'DHW Demand [L]')
            # world.connect(heatpumps[0], csv_writer, 'Q_Demand', 'Q_Supplied', 'T_amb', 'heat_source_T', 'cons_T',
            #               'P_Required',
            #               'COP', 'cond_m', 'cond_in_T', 'on_fraction')
            #
            # world.connect(ctrls[0], csv_writer, 'heat_demand', 'heat_supply', 'hp_demand', 'sh_supply', 'sh_demand',
            #               'hp_supply',
            #               'sh_in_F', 'sh_in_T', 'sh_out_F', 'sh_out_T', 'dhw_in_F', 'dhw_in_T', 'dhw_out_F',
            #               'dhw_out_T',
            #               'hp_in_F', 'hp_in_T', 'hp_out_F', 'hp_out_T', 'P_hr_sh', 'P_hr_dhw', 'dhw_demand',
            #               'dhw_supply')
            # world.connect(hwts[0], csv_writer, 'sensor_00.T', 'sensor_01.T', 'sensor_02.T', 'sensor_03.T',
            #               'sensor_04.T', 'sensor_05.T',
            #               'sh_out.T', 'sh_out.F', 'dhw_out.T', 'dhw_out.F', 'hp_in.T', 'hp_in.F', 'hp_out.T',
            #               'hp_out.F',
            #               'T_mean', 'sh_in.T', 'sh_in.F', 'dhw_in.T', 'dhw_in.F')

            # To start hwts as first simulator
            world.set_initial_event(hwts[0].sid)

        # Run
        world.run(until=END)

@pytest.mark.parametrize('same_time_loop', [False, True])
def test_example_scenarios(same_time_loop):
    example_scenario(same_time_loop)
