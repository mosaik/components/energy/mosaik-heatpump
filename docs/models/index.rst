Models
======

.. toctree::
   :maxdepth: 1
   :hidden:

   heatpump
   hotwatertank
   controller

The package contains the models for the following components of the heating system:

    1. :doc:`Heat pump <./heatpump>`
    2. :doc:`Hot water tank <./hotwatertank>`
    3. :doc:`Controller <./controller>`

