mosaik-heatpump
===============

This package provides models for the simulation of heating systems- consisting of heat pumps, hot water tanks,
and controllers - and adapters for the co-simulation of these models using *mosaik*.

Installation & Tests
--------------------

You can install mosaik-heatpump with pip:

.. code-block:: bash

      pip install mosaik-heatpump


You can run the tests with:

.. code-block:: bash

      pytest

Documentation, Source Code and Issues
-------------------------------------

The documentation is available at `Read the Docs <https://mosaik.readthedocs.io/en/latest/ecosystem/components/mosaik-heatpump/overview.html>`_.


Please report bugs and ideas for improvement to our `issue tracker`__.

__ https://gitlab.com/mosaik/components/energy/mosaik-heatpump/-/issues

Get in touch, ask questions, discuss
------------------------------------

For questions and general discussion about mosaik-heatpump, you can use
`mosaik's GitHub Discussions <https://github.com/orgs/OFFIS-mosaik/discussions>`_.

Citation
--------

The model was initially used and presented `here <https://doi.org/10.1186/s42162-021-00180-6>`_ , 
but it is not described in detail. We plan to publish a paper presenting the package, and will
update this section after the publication.

License
-------

This project is licensed under the MIT License. See `LICENSE <https://gitlab.com/mosaik/components/energy/mosaik-heatpump/-/blob/master/LICENSE>`_ file for details.
